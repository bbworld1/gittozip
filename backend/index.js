const http = require('http');
const url = require('url');
const adm_zip = require('adm-zip');
const git = require('simple-git');
const static = require('node-static');
const fs = require('fs-extra');

// When starting the server, clear any stuff that might be left over
fs.emptyDirSync("./repos/");

const fileServer = new static.Server("./");

const server = http.createServer((req, res) => {
    if (req.url.startsWith("/repos")) {
        fileServer.serve(req, res);
    } else {

        // Basically we do all the magic in here
        // Eventually return the zip as a blob
        let queryData = url.parse(req.url, true);
        if (queryData.query.url == null) {
            res.statusCode = 400;
            res.statusMessage = "Invalid URL";
            res.end();
        } else {
            res.writeHead(200, {
                'Content-Type': 'application/octet-stream',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Request-Method': '*',
            });

            let repoName = queryData.query.url.match(/[^\\\/]+(?=\.[\w]+$)|[^\\\/]+$/g)[0] + "-" + Math.random().toString(36).substring(7);
            console.log("Cloning repo", queryData.query.url);
            console.log("Repo name:", repoName);

            git('./repos/').clone(queryData.query.url, repoName, () => {
                console.log("Cloned.");
                console.log("Preparing zip file.");
                var zip = new adm_zip();

                zip.addLocalFolder('./repos/' + repoName);
                zip.writeZip('./repos/' + repoName + '.zip');

                console.log("Zip file ready.");
                res.write("http://127.0.0.1:5000/repos/" + repoName + ".zip");
                res.end();

            });
        }
    }
});

server.listen(5000, () => {
    console.log(`Server running at http://127.0.0.1:5000/`)
});
